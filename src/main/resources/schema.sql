DROP TABLE IF EXISTS STUDENTS;
create table STUDENTS
(
    ID        INTEGER auto_increment
        primary key
        unique,
    NAME      CHARACTER VARYING(100),
    LENGTH    DOUBLE PRECISION,
    BIRTHDAY  DATE
);
