package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.Student;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbctemplate")
public class JDBCTemplateStudentRepository implements StudentRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert studentInserter;

    public JDBCTemplateStudentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.studentInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("STUDENTS")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public static Student mapStudentRow(ResultSet rs, int rowid) throws SQLException {
        return new Student(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDouble("LENGTH"),
                rs.getDate("BIRTHDAY").toLocalDate());
    }

    @Override
    public List<Student> findAll() {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS", JDBCTemplateStudentRepository::mapStudentRow);
        return students;
    }

    @Override
    public Student findById(int id) {
        Student student = jdbcTemplate.queryForObject("SELECT * FROM STUDENTS WHERE ID = ?", JDBCTemplateStudentRepository::mapStudentRow, id);
        return student;
    }

    @Override
    public Student createStudent(Student student) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", student.getName());
        parameters.put("LENGTH", student.getLenght());
        parameters.put("BIRTHDAY", Date.valueOf(student.getBirthday()));
        student.setId(studentInserter.executeAndReturnKey(parameters).intValue());
        return student;
    }

    @Override
    public void updateStudent(Student student) {
        jdbcTemplate.update("UPDATE STUDENTS SET NAME=?, LENGTH=?,BIRTHDAY=? WHERE ID=?",
                student.getName(), student.getLenght(), Date.valueOf(student.getBirthday()), student.getId());
    }

    @Override
    public void deleteStudent(int id) {
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE ID=?", id);
    }
}
