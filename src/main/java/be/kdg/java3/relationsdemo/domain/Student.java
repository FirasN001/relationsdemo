package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student {
    private int id;
    private String name;
    private double lenght;
    private LocalDate birthday;

    public Student(int id, String name, double lenght, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.lenght = lenght;
        this.birthday = birthday;
    }

    public Student(String name, double lenght, LocalDate birthday) {
        this.name = name;
        this.lenght = lenght;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lenght=" + lenght +
                ", birthday=" + birthday +
                '}' ;
    }
}
